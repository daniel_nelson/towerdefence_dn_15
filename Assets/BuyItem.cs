﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuyItem : MonoBehaviour {

	public string itemName;
	public GameObject nameLabel;
	public GameObject buyButton;
	bool isBought;
	public int price;
	public GameObject priceLabel;
	private GameObject player; 

	// Use this for initialization
	void Start () {
		nameLabel.GetComponent<Text> ().text = itemName;
		priceLabel.GetComponent<Text> ().text = "$" + price;


		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8  && go.CompareTag("Player"))
				{
				player = go; 
				}
		} 



		SetButton();
	}
	
	// Update is called once per frame
	protected void Update () {
		SetButton();

		if (buyButton.GetComponent<BuyButton> ().IsBought()) {
			buyButton.SetActive (true);
		}


	}

	void SetButton(){
		Debug.Log ("Entered Shop Item " + itemName);
		if (!isBought) {
			//Check if player has enough funds
			buyButton.SetActive (true);	
			if (PlayerHasFunds()) {
				buyButton.GetComponent<BuyButton>().SetCanBuy (true);
				buyButton.GetComponent<BuyButton> ().SetPrice(price);
			} else {
				buyButton.GetComponent<BuyButton>().SetCanBuy (false);
			}
		}
	}
		

	bool PlayerHasFunds(){
		//GameObject player = GameObject.FindGameObjectWithTag ("Player");
		if (player != null) {
			return player.GetComponent<Player> ().funds >= price;
		} else {
			return false;
		}

	}
}
