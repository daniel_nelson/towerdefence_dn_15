﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections.Generic;

public class EnemySpawner : NetworkBehaviour {

	private Player player;
    public static EnemySpawner singleton;
    string currentEnemyType = Enemy.PLACEHOLDER;
    private Enemy _currentEnemy;
    public Enemy currentEnemy {
        get {
            _currentEnemy = EnemyManager.GetEnemy(currentEnemyType);
            return _currentEnemy;
        }
        set { currentEnemy = value; }
    }
    void Awake() {
        if (singleton != null) {
            Debug.LogError("More than one Enemy manager is running");
        } else {
            singleton = this;
            //singletons make it very easy to gain access to the Gamemanager without any gameobject.find or similar things. 
        }
    }

    void Update() {
        if(player == null) {
            SetupPlayer();
            Debug.Log("should be set up");
        }
        
    }
    [Command]
    void CmdDebug(string s) {
        print(s);
    }
    Vector3 UpdateSpawnPos(Vector3 centreSpawnPos, float range){
		Vector3 spawnPos;
		spawnPos.x = centreSpawnPos.x + (Random.Range (-range, range));
		spawnPos.z = centreSpawnPos.z + (Random.Range (-range, range));
		spawnPos.y = centreSpawnPos.y;
		return spawnPos;
	}
    void SetupPlayer(){
        PlayerController pc = ClientScene.localPlayers[ClientScene.localPlayers.Count - 1];
        player = pc.gameObject.GetComponent<Player>();
	}

    #region Enemy Functions
    [Command]
    public void CmdSpawnEnemy(string _type, Vector3 _pos, float _radius) {
        Enemy enemy = EnemyManager.spawnableEnemies[_type];
        
        Vector3 spawnPos = UpdateSpawnPos(_pos, _radius);
        GameObject obj = enemy.gameObject;
        obj = (GameObject)Instantiate(obj, spawnPos, Quaternion.identity);

        NetworkServer.Spawn(obj);
        string ID = obj.GetComponent<NetworkIdentity>().netId.ToString();
        obj.name = _type + " " + ID;
        enemy = obj.GetComponent<Enemy>();

        EnemyManager.enemies.Add(ID, enemy);
    }

    [Command]
    public void CmdDestroyEnemy(Vector3 _pos) {
        Enemy enemy = default(Enemy);
        string ID = "";
        foreach (KeyValuePair<string, Enemy> kp in EnemyManager.enemies) {
            if (kp.Value.transform.position == _pos) {
                enemy = kp.Value;
                ID = kp.Key;
                break;
            }
        }
        if (enemy != default(Enemy)) {
            EnemyManager.enemies.Remove(ID);
            NetworkServer.Destroy(enemy.gameObject);
        }
    }
    #endregion
}
