﻿using UnityEngine;
using UnityEngine.Networking;

public class Base : BaseHealth {

	void Awake () {
		currentHealth = maxHealth;
	}
    public override void OnStartClient() {
        base.OnStartClient();
        currentHealth = maxHealth;

    }
    [Command]
    void CmdDebug(string s) {
        print(s);
    }
		
	protected override void Die(){
        //here goes explode stuff + stuff which should happen after explosion
        CmdDebug("baseded");
		GameManager.state = GameManager.GameState.LOST;       
		base.Die ();
	}
}
