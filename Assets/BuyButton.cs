﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BuyButton : MonoBehaviour {
	bool isBought;
	bool canBuy;
	int price;
	public GameObject labelText;
	public Button button;

	private GameObject player; 

	// Use this for initialization
	void Start () {
		labelText.GetComponent<Text> ().text = "Buy";

		GameObject[] gos = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[]; //will return an array of all GameObjects in the scene
		foreach(GameObject go in gos)
		{
			if(go.layer == 8 && go.CompareTag("Player"))//go.layer=="LocalPlayer" && go.CompareTag("Player"))
			{
				player = go; 
			}
		} 

	}

	public void SetCanBuy(bool isBuyable){
		canBuy = isBuyable;
		if (canBuy) {
			labelText.GetComponent<Text> ().text = "Buy";
			button.interactable = true;
		} else {
			//this.GetComponent<Image> ().color = Color.red;
			labelText.GetComponent<Text> ().text = "Unavailable";
			button.interactable = false;
		}
	}

	public void SetPrice(int price){
		this.price = price;
	}

	// Update is called once per frame
	void Update () {
		
	}


	protected virtual void OnMouseDown(){
		//GameObject player = GameObject.FindGameObjectWithTag ("Player");
		if (player != null) {
			player.GetComponent<Player> ().funds -= price;
		}
		labelText.GetComponent<Text> ().text = "Bought";
		button.interactable = false;
		//Overridden in child classes
	}


	public bool IsBought(){
		return isBought;
	}
}
